(story title) La Princesse spéculaire
(story author) Nathanaël Marion
(story noun) Une fiction interactive
(story release 2)
(story ifid) 1F47EA6F-E7D6-4186-AE44-13F769C9A4A4
(story blurb)
	Après la mort de ma mère, j'ai découvert dans son journal que les contes qu'elle me racontait quand j'étais enfant avaient un fond de vérité.
	(par)
	C'est ce qui m'a lancé sur les traces de la mystérieuse Princesse spéculaire.

%% TODO-cadratin Ce qui suit n'est pas inclus dans Dialog, mais on le fournit quand même.
%% (story creation year 2022)
%% (story genre) Fantastique

(style class @poème)
	font-style: italic;
	text-align: center;

(style class @titre)
	font-size: 2.2rem;
	font-weight: bold;
	line-height: 1.2;
	margin-top: 0.5em;
	margin-bottom: .5em;
	text-align: center;

(style class @sous-titre)
	font-size: 2rem;
	font-weight: bold;
	line-height: 1.2;
	margin-top: 0.5em;
	margin-bottom: .5em;
	text-align: center;

(style class @auteur)
	margin-bottom: 2em;
	text-align: center;


(story viewpoint #first-singular)

(intro)
	(div @titre) { La Princesse spéculaire }
	(div @auteur) { Nathanaël Marion }

	- Commencer (line)
	- Charger une partie
	(par)
	Note : Ce jeu est, à ma connaissance, le premier jeu en français écrit avec Dialog. Il est donc possible que vous trouviez des textes que j'ai oublié de traduire. De plus, le gros du jeu a été écrit en 2 jours et il est donc probable qu'il contienne des bugs.
	(par)
	Je ne vous demande bien sûr pas de noter le jeu moins sévèrement dans le cadre du concours à cause de cela ! Simplement, si vous rencontrez des problèmes, vous en connaîtrez les raisons.

	*(repeat forever)
	(par)
	(prompt and input $Words)
	(main menu option $Words)

(main menu option [commencer])
	(clear)
	(div @sous-titre) {
		Première partie (line)
		Les ruines
	} %% /sous-titre
	(div @poème) {
		Quand les gens se révoltèrent, (line)
		Que le tyran fut jeté aux fers, (line)
		Alors ils se demandèrent : (line)
		Qui pour gouverner leur terre ? (line)
		La miroitière, un peu sorcière, (line)
		Répondit à leurs prières (line)
		Confectionna une œuvre spectaculaire (line)
		Une dirigeante d'argent et de verre (line)
		Qui serait le reflet de tous ses pairs (line)
		Et qu'elle nomma la Princesse spéculaire.
	} %% /poème
	(par)
	(any key)
	(clear)
	Ma mère était la seule personne qui comptait pour moi — je n'avais jamais connu mon père, qui l'avait quittée avant ma naissance. Et puis il y a eu cet accident de voiture stupide.
	(par)
	(any key)
	Pendant longtemps après cet événement, je n'ai rien fait d'autre que penser à elle. Je me remémorais mes jeunes années, nos moments favoris quand elle me racontait des contes ou récitait des poèmes à propos de ce royaume lointain et de l'étrange princesse à sa tête.
	(par)
	(any key)
	Qand j'ai enfin eu le courage de regarder dans les affaires qu'elle m'avait léguées, j'ai découvert son journal, empli de détails et de cartes sur ce royaume qui avait peuplé mon enfance.
	(par)
	(any key)
	Étaient-ce bien plus que des contes et des poèmes ? J'ai décidé de suivre l'une des cartes qu'elle avait dessinées. Pas vraiment parce que j'y croyais, mais davantage comme un pèlerinage, afin de faire mon deuil. Ce qui m'a menée ici.
	(par)
	(any key)
	(clear)
	(try [look])

(main menu option $Words)
	{
		($Words = [charger])
		(or)($Words = [charger partie])
		(or)($Words = [charger une partie])
	}
	(stoppable) (try [restore])
	(just)(fail)


(main menu option $)
	Veuillez taper l'une des optons proposées.
	(fail)

%% TODO : Ajouter des `(notice $)` après les descriptions et actions qui le nécessitent.

%%% Le joueur

#joueur

(current player *)
(descr *)
	Je suis seule, un peu triste, mais déterminée à résoudre les mystères que ma mère a laissé derrière elle. Ou au moins trouver la paix.

#journal

(item *)
(* is #heldby #joueur)
(* is handled)
(indefinite article *) le
(name *) journal de ma mère
(dict *) mere cahier
(descr *)
	J'ai trouvé ce cahier, un peu jauni par les années, dans les affaires de ma mère, après son fatal accident. Jamais je ne me serais doutée qu'elle cachait tous ces secrets.

(perform [read *])
	Le cahier est empli de l'écriture minuscule mais régulière de ma mère. Y sont décrits les détails de ce que je croyais n'être qu'un conte, ainsi que l'emplacement supposé de la sépulture de la Princesse spéculaire. Il est la raison de ma venue ici.
	(select)
		(par)
		Un éclat de miroir terni a été collé avec du ruban adhésif sur la troisième de couverture. Pressentant qu'il me sera utile, je le décolle avec douceur ; heureusement, la ruban adhésif ne tient plus beaucoup.
		(now)(#éclat is #heldby #joueur)
		(now)(#éclat is handled)
	(or)(stopping)

(instead of [open *])
	(try [read *])

#éclat

(item *)
(name *) éclat de miroir
(dict *) eclat fragment
(heads *) eclat fragment %% Pour privilégier le miroir plutôt que l'éclat.
(descr *)
	(if)(éclat is polished)(then)
		Par un phénomène que je ne saurais expliquer, l'éclat brille de nouveau, comme s'il venait d'être fabriqué.
	(else)
		La ternissure de ce fragment témoigne de son âge.
	(endif)
	L'un des côté forme un arc de cercle, souvenir du rebord du miroir auquel il appartenait.

(prevent [clean *])
	(if)(éclat is polished)(then)
		C'est inutile : le fragment de miroir a déjà retrouvé son lustre.
	(else)
		Je frotte un instant l'éclat de miroir mais rien n'y fait, il est toujours aussi terni.
	(endif)

%%% La forêt

#forêt

(f *)
(room *)
(from * go #east to #place)
(name *) forêt
(dict *) foret
(room header *) Une forêt en automne
(look *)
	Il a plu quelques instants plus tôt, mais il fait beau maintenant et l'endroit est apaisant sous les feuilles qui tombent l'odeur du pétrichor. Je m'arrête un instant et prend une grande respiration.
	(par)
	(if)(#place is visited)(then)
		La chapelle, raison de ma venue ici, est plus à l'est.
	(else)
		À en croire le journal de ma mère, que je garde précieusement sur moi, mon but ne devrais plus être très loin à l'est.
	(endif)

(#joueur is #in *)

%% TODO message aller nulle part.

#arbres

(plural *)
(* is #in #forêt)
(name *) arbres
(dict *) arbre feuille feuilles
(descr *)
	Les arbres n'ont pas encore perdu toutes leurs feuilles et, bien qu'il luisent encore de la récente pluie, ils embrasent la forêt de leurs couleurs.

%%% Devant la fontaine

#place

(proper *)
(room *)
(from * go #west to #forêt)
(from * go #east to #nef)
(from * go #northeast to #nord-chapelle)
(from * go #southeast to #sud-chapelle)
(name *) près de la fontaine
(room header *) Près d'une fontaine délabrée
(look *)
	La forêt s'éclaircit pour laisser place à un terrain herbeux. Près d'un arbre repose ce qui reste d'une fontaine et plus loin, à l'est, au pied d'un rocher, s'étendent les ruines de ce qui semble avoir été une chapelle.

#fontaine

(container *)
(* is #in #place)
(name *) fontaine délabrée
(dict *) delabree
(descr *)
	(if)(#mercure is #in *)(then)
		Comme par miracle, la fontaine fonctionne ! Mais le liquide brillant qu'elle contient ne me dit rien qui vaille.
	(else)
		Cette viellie fontaine de pierre brute gît à l'ombre d'un arbre. Elle a dû connaître de meilleurs jours, néanmoins elle est relativement en bon état au vu de son âge probable.
	(endif)

(appearance *)
	(#mercure is #in *)
	(select)
		Je cligne des yeux en m'approchant de la fontaine : elle s'est remise en marche ! Mais elle n'est pas remplie d'eau. Il s'agit plutôt d'un liquide gris brillant.
	(or)
		La fontaine est remplie d'un étrange liquide gris brillant.
	(stopping)

(appearance *)
	(éclat is polished)
	(#éclat is pristine)
	(select)
		Un objet étincelant brille au fond de la fontaine. M'approchant, je me rend compte qu'il s'agit de mon éclat de miroir, qui est maintenant comme neuf !
	(or)
		Un objet étincelant brille au fond de la fontaine : mon éclat de miroir, maintenant comme neuf.
	(stopping)

(instead of [put $O #in *])
	(#mercure is #in *)
	(try [put $O #in #mercure])

#fontaine-intérieur

%% Endroit de stockage temporaire pour les objets qui était dans la fontaine avant que la valve soit ouverte.

(room *)

#mercure

(out of reach *) %% Pour bloquer les actions requérant un objet touchable.
(name *) mercure
(dict *) liquide gris brillant argent vif-argent
(descr *)
	La fontaine est maintenant remplie d'un liquide brillant. Du mercure ? Non, c'est impossible, ce que je vois ne peut être vrai.

(narrate when * is out of reach)
	Si c'est étrange liquide est du mercure, alors je préfère ne pas trop m'approcher. Et si c'est autre chose… alors il m'inspire encore moins confiance.

#branche

(f *)
(item *)
(* is #in #place)
(name *) longue branche
(descr *)
	Cette longue branche, fine et solide, vient sûrement de l'arbre près de la fontaine.

(appearance *)
	(* is pristine)
	Une longue branche gît près de la fontaine.

#arbre

(plural *)
(* is #in #place)
(name *) arbre
(descr *)
	Cette arbre se dresse fièrement à côté de la fontaine, la protégeant de son ombre.

%%% Zone au nord de la chapelle, près des stèles

#nord-chapelle

(plural *)
(room *)
(from * go #southwest to #place)
(from * go #south to #nef)
(from * go #up to #sommet)
(name *) stèles
(room header *) Près de stèles
(look *)
	Deux pierres dressées sont plantées ici, au nord de la chapelle, et témoignent de l'importance que l'on portait à ce lieu bien avant la construction de l'édifice. Le rocher, tout proche, surplombe la scène. Un escalier permettant d'atteindre le sommet y a été taillé.

#escalier

(#nord-chapelle/#sommet attracts *)
(name *) escalier
(dict *) escaliers marche marches
(descr *)
	Des marches ont été taillées à même le rocher.
	(if)(#sommet is unvisited)(then)
		Elles semblent encore praticables, mais l'ascension ne doit pas être de tout repos.
	(else)
		Elles sont praticables, mais l'ascension n'est pas de tout repos.
	(endif)

(instead of [climb *])
	(current room #nord-chapelle)
	(try [go #up])

(instead of [leave *])
	(current room #sommet)
	(try [go #down])

#stèles

(f *)
(plural *)
(* is #in #nord-chapelle)
(name *) stèles
(dict *) stele steles pierre pierres
(descr *)
	Deux stèles couvertes d'entrelacs et autres symboles abstraits se tiennent ici.
	(if)(#morceau-vert is #partof #stèles)(then)
		Un morceau de verre vert est serti dans l'une d'elles.
	(endif)

#morceau-vert

(item *)
(vitrail *)
(* is #partof #stèles)
(name *) morceau de verre vert
(heads *) morceau
(descr *)
	Ce morceau de verre de couleur verte provient sûrement d'un vitrail.
	(if)(* is #partof #stèles)(then)
		Il est serti dans l'une des stèles.
	(endif)

(prevent [take *])
	(* is #partof #stèles)
	(if)(#burin is #heldby #joueur)(then)
		Grâce au burin, je parviens à retirer le morceau de verre vert de la stèle, heureusement en un seul morceau.
		(now)(* is #heldby #joueur)
		(now)(* is handled)
	(else)
		Le morceau de verre semble ne faire qu'un avec la stèle et je n'arrive pas à le retirer.
	(endif)

(instead of [attack *])
	(* is #partof #stèles)
	(try [take *])

(instead of [attack #stèles])
	(* is #partof #stèles)
	(#burin is #heldby #joueur)
	(try [take *])

%%% Le sommet

#sommet

(room *)
(from * go #down to #nord-chapelle)
(name *) sommet du rocher
(look *)
	(select)
		Après une dangereuse ascension sur des marches usées, je m'arrête sur une plate-forme au sommet du rocher pour reprendre mon souffle. J'embrasse le paysage, mer de feuillages rouge et or oscillant sous le vent, et l'énergie me revient. Je me retourne, et une étrange vanne en argent, fixée dans la roche, attire mon regard.
	(or)
		D'ici, on domine la forêt. Je n'ai pas le vertige, mais le terrain accidenté et le vent me font souhaiter de redescendre rapidement. Près du centre de la plate-forme, une vanne en argent directement fixée dans la roche, brille.
	(stopping)

(prevent [leave * #west])
	Je ne peux pas me diriger directement à la chapelle ou je vais me rompre le cou. Il faut que je redescende par l'escalier.

#paysage

(f *)
(out of reach *)
(* is #in #sommet)
(name *) forêt
(dict *) foret arbre arbres paysage
(descr *)
	Le paysage, océan rouge et doré sous un ciel limpide, est magnifique. Je comprends pourquoi les anciens ont choisi cet endroit pour bâtir la chapelle. Je m'arrête un instant, avant qu'une bourrasque me ramène à la réalité.

#vanne

(f *)
(* is #in #sommet)
(name *) vanne
(dict *) valve
(descr *)
	La vanne en argent brille de manière saugrenue. Qui peut bien la maintenir en état ?

(instead of [turn *])
	(if)(vanne is open)(then)
		(try [close *])
	(else)
		(try [open *])
	(endif)

(prevent [open *])
	(if)(vanne is open)(then)
		Le vanne ne tourne pas plus dans ce sens.
	(else)
		(select)J'ouvre(or)Je rouvre(stopping) la vanne. Du moins c'est ce que je pense, car rien ne se produit.
		(now)(vanne is open)
		%% On retire tout ce qui est dans la fontaine.
		(exhaust) {
			*($O is #in #fontaine)
			(now)($O is #in #fontaine-intérieur)
		}
		%% Puis on fait apparaître le mercure.
		(now)(#mercure is #in #fontaine)
		%% Et enfin on regarde si on doit rendre son éclat à l'éclat. :)
		(if)(#éclat is #in #fontaine-intérieur)(then)
			(now)(éclat is polished)
			(now)(#éclat is pristine)
		(endif)
	(endif)

(prevent [close *])
	(if)(vanne is open)(then)
		Je referme la vanne. Du moins c'est ce je pense, car rien ne se produit.
		(now)~(vanne is open)
		%% On remet tout ce qui était dans la fontaine avant l'ouverture.
		(exhaust) {
			*($O is #in #fontaine-intérieur)
			(now)($O is #in #fontaine)
		}
		%% Et on retire le mercure.
		(now)(#mercure is nowhere)
	(else)
		La vanne ne tourne pas plus dans ce sens.
	(endif)

#grille

%% La grille est mise dans le sommet quand on tire le levier dans la colline.

(f *)
(supporter *)
(name *) grille
(dict *) ouverture grillage grillagee trou
(descr *)
	(select)
		Je me penche au-dessus de la grille. Plissant les yeux, j'aperçois le scintillement du levier en argent en contrebas. L'ouverture dans le plafond donne donc bien ici, sur le sommet.
	(or)
		Il s'agit de l'ouverture de la grille que j'ai fait apparaître en tirant le levier dans le rocher.
	(stopping)

(appearance *)
	Une ouverture grillagée est apparue au centre de la plate-forme.

(prevent [attack *])
	Les barreaux, bien qu'anciens, sont bien trop épais.

#morceau-rouge

(item *)
(vitrail *)
(* is #on #grille)
(name *) morceau de verre rouge
(heads *) morceau
(descr *)
	Ce morceau de verre de couleur rouge provient sûrement d'un vitrail. Il
	(if)(* is #on #grille)(then)
		est
	(else)
		était
	(endif)
	assez grand pour ne pas tomber à travers les barreaux de la grille.

(appearance *)
	(* is pristine)
	Un morceau de verre rouge est posé sur la grille. C'est lui qui causait la lueur rougeâtre dans le rocher.

%%% Zone au sud de la chapelle, près du calvaire

#sud-chapelle

(room *)
(from * go #northwest to #place)
(from * go #north to #nef)
(name *) calvaire
(room header *) Devant le calvaire
(look *)
	Le terrain au sud des ruines est une étroite bande coincée entre la forêt et le rocher. Un calvaire, en bien meilleur état que l'édifice, s'y dresse près de la lisière.

#calvaire

(* is #in #sud-chapelle)
(name *) calvaire
(dict *) croix
(descr *)
	Ce calvaire de deux mètres, gravée d'entrelacs, domine une dalle de pierre rectangulaire, vraisemblablement une tombe. À la base de la croix, juste au-dessus de la pierre tombale,
	(if)(#miroir is #partof #tombe)(then)
		a été incrusté un miroir circulaire étincelant.
	(else)
		se trouve un creux circulaire où le miroir avait été fixé.
	(endif)

#tombe

(f *)
(* is #in #sud-chapelle)
(name *) tombe
(dict *) plaque pierre
(descr *)
	Une dalle de pierre sans ornementations ni gravures, et très ancienne au vu de son état, a été posée au pied du calvaire
	(if)(#miroir is #partof *)(then)
		, juste sous un miroir circulaire
	(endif)
	. Je me recueille un instant, me demandant qui repose ici.
	(if)~(#miroir is #partof *)(then)
		Serait-ce la femme qui est apparue dans ma vision ? Était-elle la miroitière magicienne ?
	(endif)

#miroir

(item *)
(* is #partof #tombe)
(name *) miroir
(dict *) rond circulaire
(dict *) (#pied is nowhere) *(dict #pied)
(heads *) miroir %% Pour le privilégier plutôt que l'éclat.
(descr *)
	(if)(* is #partof #tombe)(then)
		Un miroir rond est fixé à la base du calvaire, au-dessus de la tombe. Aucune poussière, aucune ternissure, aucune fêlure ; son état remarquable constraste avec son environnement. Seul un fragment manquant sur le bord vient briser sa beauté.
	(elseif)(#pied is nowhere)(then)
		Le miroir est maintenant attaché au pied trouvé dans les décombres de la chapelle. Ces deux objets semblent avoir été faits l'un pour l'autre.
		(if)(* is #on #autel)~(paroi is open)(then)
			(par)
			Le soleil tombe sur le soleil ici, mais le miroir n'est pas dans le bon sens.
		(endif)
	(else)
		Le fragment trouvé dans le journal de ma mère a mystèrieusement fusionné avec le miroir circulaire, lui rendant son intégrité. (select) Mais quel est donc cet endroit ? (or)(stopping)
	(endif)

(appearance *)
	(#pied is nowhere)
	(* is #on #autel)
	Le miroir est debout, le pied bien encastré dans l'autel.

(prevent [take *])
	(* is #partof #tombe)
	J'essaie de retirer le miroir de la base du calvaire, mais mes efforts sont vains, il est trop bien incrusté.

(prevent [put #éclat #in/#on *])
	(if)(éclat is polished)(then)
		Je m'accroupis et insère l'éclat sur le côté du miroir. Une bourrasque se lève, la surface du miroir s'obscurcit.
		(par)
		(any key)
		Une image se forme. Floue d'abord, elle gagne rapidement en netteté. Une femme âgée grave au burin des symboles et un cercle sur une surface rocheuse, avec en arrière-plan l'intérieur de la chapelle, telle qu'elle devait être autrefois. Une tâche de lumière vient éclairer le motifs nouvellement gravés, sans que j'en discerne l'origine. L'image disparaît.
		(par)
		(any key)
		Je me frotte les yeux. Le miroir est à terre, et en un seul morceau : le seul signe que quelque chose d'étrange se soit véritablement passé.
		(now)(* is #in #sud-chapelle)
		(now)(#éclat is nowhere)
	(else)
		(select)
			Je m'accroupis et insère l'éclat sur le côté du miroir. Il entre parfaitement mais rien ne se passe. Je secoue la tête. À quoi m'attendais-je ? À ce que la tombe s'ouvre, révélant un escalier menant à un inestimable trésor archéologique ? Je suis ridicule.
			(par)
			De toute façon, l'éclat est tellement terni qu'il ne fait pas honneur au reste du miroir, et je le reprends. Me relevant, je me demande comment ma mère a-t-elle bien pu entrer en sa possession.
		(or)
			Bien évidemment, aucun événement particulier ne se déclenche, comme (select) la fois précédente (or) les fois précédentes (stopping).
		(stopping)
	(endif)

(instead of [tie #éclat to *])
	(try [put #éclat #in *])

(instead of [put #éclat #in/#on #calvaire/#tombe])
	(try [put #éclat #in *])

(prevent [turn *])
	(#pied is nowhere)
	(* is #on #autel)
	(if)~(paroi is open)(then)
		Je fais pivoter le miroir afin que le soleil soit réfléchi sur le cercle gravé dans la paroi. Il ne se passe rien au début, puis le cercle et les caractères en dessous se mettent à luire. Le sol vibre.
		(par)
		(any key)
		Un interstice apparait dans la paroi. Il s'élargit, comme une gueule béante s'ouvrant sur un sombre couloir.
		(par)
		(any key)
		Je frissonne. Il va vraiment falloir que je me fasse à l'idée que cet endroit n'est pas normal.
		(now)(paroi is open)
		(any key)
	(else)
		Je m'amuse à éclaire diverses choses autour de moi avec le miroir, mais rien de notable ne se produit.
	(endif)

%%% La nef

#nef

(f *)
(room *)
(from * go #west to #place)
(from * go #east to #chœur)
(from * go #north to #nord-chapelle)
(from * go #south to #sud-chapelle)
(name *) nef
(room header *) Dans ce qu'il reste de la nef
(look *)
	(select)
		Je pénètre dans les ruines de la nef, contournant les gravats qui jonchent le sol.
	(or)
		De nombreux gravats jonchent le sol de ce qu'il était la nef.
	(stopping)
	Les murs nord et sud, depuis longtemps disparus, ne bloquent plus le passage vers l'extérieur. À l'est, le chœur minuscule est écrasé par la masse du rocher.

#décombres

(plural *)
(* is #in #nef)
(name *) décombres
(dict *) decombre decombres gravat gravats
(descr *)
	Le temps a pris les murs et le plafond, ne daignant laisser à la chapelle que quelques gravats.
	(if)(#pied is pristine)(then)
		(par)
		Un cylindre en métal dépassant d'entre deux blocs attire mon regard. (select) Je fronce les sourcils. (or)(stopping) Que peut bien être cet objet ?
	(endif)

(prevent [push *])
	(if)(#pied is pristine)(then)
		(if)(#branche is #heldby #joueur)(then)
			(now)(#pied is #heldby #joueur)
			(now)(#pied is handled)
			Me servant de la branche comme d'un levier, je parviens à déplacer l'une des pierres juste assez pour me saisir de l'objet brillant. On dirait une sorte de support en métal.
		(else)
			J'essaie de dégager l'objet métallique, mais les gravats sont trop lourds. Il me faudrait un levier ou quelque chose de similaire.
		(endif)
	(else) %% (#pied is handled)
		Il ne semble pas y avoir autre chose d'intéressant dans les gravats.
	(endif)

#pied

(item *)
(* is #in #nef)
(name *) (* is pristine) objet métallique
(name *) pied en métal
(dict *) objet metallique brillant support metal cylindre
(descr *)
	(if)(* is pristine)(then)
		Un cylindre en métal dépasse d'entre deux blocs. Impossible d'en savoir plus tant qu'il est coincé.
	(else)
		Il s'agit d'un cylindre surmonté d'un demi-cercle pivotant, le tout en métal. L'objet me rappelle un peu un support pour globe terrestre. Pour une raison que je ne saurais expliquer, il est comme neuf. Quelqu'un serait-il venu ici avant moi ?
	(endif)

(appearance *)
	(* is pristine)
	Un objet métallique brillant attire mon regard dans les décombres.

(appearance *)
	(* is #on #autel)
	Le pied en métal est debout, bien encastré dans l'autel.

(prevent [take *])
	(* is pristine)
	L'objet est coincé entre deux morceaux de pierre et mes efforts sont vains.

(instead of [tie $A to $B])
	{ ($A = #miroir)($B = *) (or) ($A = *)($B = #miroir) }
	(* is handled)
	(if)(#miroir is #partof #tombe)(then)
		C'est impossible tant que le miroir est fixé à la base du calvaire.
	(else)
		J'insère le miroir dans l'arc du pied. Il s'y ajuste parfaitement, et je dispose maintenant d'un miroir orientable.
	(endif)
	(now)(#pied is nowhere)
	(now)(#miroir is #heldby #joueur)
	(now)(#miroir is handled)

(prevent [put #miroir #on/#in *])
	(* is handled)
	(try [tie #miroir to *])

%%% Le chœur

#chœur

(room *)
(from * go #west to #nef)
(from * go #east to #couloir) (paroi is open)
(from * go #in to #east) (paroi is open)
(name *) chœur
(room header *) Près de l'autel
(look *)
	La chapelle a été construite contre la paroi du rocher, faisant office de mur à l'est.
	(select)
		Le danger que cela représente n'a visiblement pas dérangé ni ses bâtisseurs, ni les fidèles. Mes yeux se posent sur l'autel éclairé par le soleil, et j'imagine un instant ce que cet endroit devait être auparavant.
	(or)
		L'autel, devant l'éminence, se fait tout petit.
	(stopping)
	(if)(paroi is open)(then)
		(par)
		À l'est, la paroi rocheuse s'est ouverte sur un sombre couloir.
	(endif)

#autel

(supporter *)
(* is #in #chœur)
(name *) autel
(dict *) trou
(descr *)
	C'est un simple bloc de pierre grise polie par les années. Au centre a été percé un trou de la largeur d'un doigt
	(if)~(#pied is #on #autel) (or) (#pied is nowhere)~(#miroir is #on #autel)(then)
		, comme pour accueillir quelque chose
	(endif)
	.

(instead of [put #pied #in *])
	(try [put #pied #on *])

(instead of [put #miroir #in *])
	(#pied is nowhere)
	(try [put #miroir #on *])

(narrate putting #pied #on *)
	J'insère le pied dans l'autel ; il entre parfaitement dans le trou.

(narrate putting #miroir #on *)
	(#pied is nowhere)
	J'insère le miroir dans l'autel ; son pied entre parfaitement dans le trou.

(narrate taking #pied)
	(#pied is #on *)
	Je retire le pied de l'autel.

(narrate taking #miroir)
	(#pied is nowhere)
	(#miroir is #on *)
	Je retire le miroir de l'autel.

#morceau-jaune

(item *)
(vitrail *)
(* is #on #autel)
(name *) morceau de verre jaune
(heads *) morceau
(descr *)
	Ce morceau de verre de couleur jaune provient sûrement d'un vitrail. Ses bords sont émoussés.

(appearance *)
	(* is pristine)
	Un morceau de verre jaune traîne sur l'autel.

#paroi

(f *)
(* is #in #chœur)
(name *) paroi rocheuse
(dict *) pierre roche rocher caractere caracteres mur
(descr *)
	(if)~(paroi is open)(then)
		La paroi rocheuse fait office de mur du fond, juste derrière l'autel. Un cercle gros comme un poing y a été gravé à hauteur d'yeux. Et en dessous, des caractères, rendus illisibles par le temps.
	(else)
		La paroi s'est ouverte en deux, dévoilant un sombre couloir creusé dans la roche.
		(if)(#couloir is unvisited)(then)
			Il va falloir prendre mon courage à deux mains et m'y enfoncer.
		(endif)
	(endif)

(perform [read *])
	(if)(paroi is open)(then)
		Les caractères ne sont plus accessibles maintenant que la paroi est ouverte.
	(else)
		L'érosion est malheureusement passée avant moi.
	(endif)

(instead of [enter #paroi])
	(paroi is open)
	(try [go #east])

%%% Le couloir sous le rocher

#couloir

(room *)
(from * go #west to #chœur)
(from * go #out to #west)
(from * go #east to #tombeau)
(name *) couloir
(room header *) Un passage s'enfonçant dans la roche
(look *)
	(select)
		%% Changer quand le tombeau est éclairé.
		Je m'avance dans le rocher. La pénombre s'épaissit au fur et à mesure que je m'enfonce dans le couloir de pierre brute, qui descend en pente douce jusqu'à devenir obscurité complète plus loin à l'est. Mon pouls s'accélère.
	(or)
		%% Changer quand le tombeau est éclairé.
		Le couloir s'enfonce en pente douce dans le rocher, si profondément que
		(if)(#miroir is #on #autel)(then)
			ni la lumière provenant du miroir ni
		(endif)
		les rayons du soleil ne parviennent pas jusqu'à son extrémité, à l'est.
	(stopping)

(narrate leaving #chœur #east)
	(* is unvisited)
	(clear)
	(div @sous-titre) {
		Deuxième partie (line)
		Le tombeau
	}
	(div @poème) {
		Plusieurs années s'écoulèrent (line)
		Dans ce royaume égalitaire. (line)
		Mais ressentir la vie des autres est un enfer. (line)
		C'est pourquoi la Princesse spéculaire, (line)
		S'enfonça profondément dans la pierre. (line)
		Ainsi, privée de toute lumière, (line)
		Elle disparut de la terre.
	} %% /poème
	(par)
	(any key)
	(clear)

%%% Le tombeau

#tombeau

(room *)
(inherently dark *) ~(ceiling is open)
(from * go #west to #couloir)
(from * go #out to #west)
(from * go #north to #salle)
(name *) tombeau %% TODO Nom quand on ne sait pas que c'est un tombeau.
(room header *) (ceiling is open) La sépulture de la princesse
(room header *) Au cœur du rocher
(darkness headline) (current room *) (room header *)
(look *)
	C'est ici, au cœur du rocher, qu'a été inhumée celle que les contes appellent la Princesse spéculaire. Un incroyable gisant au centre de la pièce témoigne de l'importance qu'on lui portait. La lumière provenant du nord et de l'ouest n'éclaire pas beaucoup l'endroit, mais est suffisante pour distinguer les traits de la statue, et sa particularité.
(narrate darkness)
	(current room *)
	L'obscurité est presque totale.
	(select)
		Quand mes yeux s'y habitue enfin, j'aperçois un infime filet de lumière s'échappant d'un passage plus loin au nord. Et bien sûr, le soleil derrière moi tente en vain de pénétrer en ce lieu depuis l'ouest.
	(or)
		Seul un infime filet de lumière s'échappe d'un passage plus loin au nord, et le soleil tente en vain de pénétrer en ce lieu depuis l'ouest.
	(stopping)

(narrate entering *)
	(ceiling is open)
	(select)
		Je m'arrête sur le seuil, ébahie. La lumière provenant de l'autre pièce me permet de découvrir les lieux : un tombeau.
		(par)
		(any key)
		(try [look])
		(stop)
	(or)
		(fail) %% Fallback to the default response.
	(stopping)

#gisant

(* is #in #tombeau)
(name *) gisant
(dict *) femme princesse speculaire statue
(descr *)
	Le gisant est… fait en miroir ? Sculpté dans du miroir ? Je ne trouve (select)(or) toujours (stopping) pas les mots pour le décrire.
	(select)
		Je m'approche pour en admirer les détails.
	(or)(stopping)
	(par)
	La femme représentée est jeune et son expression, neutre mais paisible. Tout son corps est fait d'un étrange matériau réfléchissant, dénué de poussière et totalement épargné par le temps.
	(select)
		(par)
		J'effleure la statue et retire ma main aussitôt. Elle est glacé, certes, mais c'est une étrange sensation dans mon esprit qui m'a le plus dérangée. Je me détourne avec l'impression qu'il me reste quelque chose à faire, et avec le sentiment gênant d'être observée.
	(or)(stopping)

%% TODO-cadratin : J'ai un peu peur que cela puisse poser problème avec le parser, alors on va juste faire semblant que le gisant n'est pas dans le scope avec la règle plus bas. Il faudra demander sur intfiction.org.
%% (* is in scope)
%% 	(current room #tombeau)
%% 	~(player can see)
%% 	(just)(fail)

(refuse $Action)
	(current room #tombeau)
	~(player can see)
	(* is one of $Action)
	%% On remplace le gisant dans l'action par une liste vide, pour faire comme si le gisant n'existait pas.
	(collect $Element)
		*($Original is one of $Action)
		(if)($Original = *)(then)
			($Element = [])
		(else)
			($Element = $Original)
		(endif)
	(into $ProcessedAction)
	\[J'ai seulement compris que vous vouliez (describe action $ProcessedAction).\]
	%% TODO-cadratin : remplacer par le narration predicate associé au message quand il y en aura un.

%%% La salle dans le rocher

#salle %% Faute d'un meilleur nom.

(f *)
(room *)
(from * go #south to #tombeau)
(from * go #out to #south)
(name *) salle dans le rocher %% TODO
(room header *) Salle dans le rocher
(look *)
	(if)(ceiling is open)(then)
		Je me trouve dans une étroite pièce avec un très haut plafond et éclairée par une ouverture grillagée. Sur le mur du fond au-delà du levier, derrière une table, a été placé un vitrail.
	(else)
		Cette endroit est à peine éclairé par quelques rayons rougeâtres venant d'une toute petite ouverture, loin en haut. Cette lumière fait scintiller un levier argenté.
	(endif)

#levier

(* is #in #salle)
(name *) levier argenté
(dict *) argente
(descr *)
	Un levier argenté, semblable aux autres objets que j'ai trouvés dans les ruines, attend au centre d'une flaque de lumière
	(if)(#morceau-rouge is #on #grille)(then)
		rougeâtre
	(elseif)(#morceau-vert is #on #grille)(then)
		verdâtre
	(elseif)(#morceau-jaune is #on #grille)(then)
		jaunâtre
	(endif)
	tombée
	(if)(ceiling is open)(then)
		d'une ouveture grillagée au plafond
	(else)
		d'une minuscule ouverture au plafond
	(endif)
	.

(prevent [pull *])
	(if)(ceiling is open)(then)
		J'ai déjà tiré le levier, ce qui avait élargi l'ouverture au plafond.
	(else)
		Je tire le levier, qui bascule sans le moindre effort. Le rocher vibre tandis que l'ouverture au plafond s'élargit, dévoilant une grille et au-delà, le ciel se hâtant de déverser ses rayons dans la pièce. Curieusement, j'ai l'impression que la lumière est un peu rougeâtre.
		(now)(ceiling is open)
		(now)(#grille is #in #sommet)
		(try [look])
	(endif)

(prevent [push *])
	(if)(ceiling is open)(then)
		La pensée de pousser le levier me vient, mais je la laisse repartir aussi vite. Je préfère avoir la lumière ici.
	(else)
		Je devrais plutôt le tirer.
	(endif)

#ouverture

(f *)
(out of reach *)
(from #salle go #up to *)
(name *) ouverture au plafond
(dict *) lumiere
(dict *) (ceiling is open) grille grillagee ciel
(descr *)
	(if)(ceiling is open)(then)
		L'ouverture grillagée au plafond est assez large pour éclairer l'endroit et permet à un peu d'air frais d'entrer. Et voir un morceau de ciel bleu me rassure. J'imagine qu'elle donne sur le sommet du rocher ? %% Ne plus poser la question quand on a vu la grille dans le sommet.
		%% TODO : Message s'il y a plus d'un morceau sur la grille.
		(if)(#morceau-rouge is #on #grille)(then)
			(par)
			Observant un peu mieux j'aperçois une lueur rougeâtre tout là-haut.
		(elseif)(#morceau-vert is #on #grille)(then)
			(par)
			Observant un peu mieux j'aperçois une lueur verdâtre tout là-haut.
		(elseif)(#morceau-jaune is #on #grille)(then)
			(par)
			Observant un peu mieux j'aperçois une lueur jaunâtre tout là-haut.
		(endif)
	(else)
		%% Si le plafond n'est pas ouvert, c'est forcément le morceau rouge.
		Une minuscule ouverture perce le haut plafond, et permet à quelques rayons extérieurs, et curieusement rougeâtres, de chasser l'obscurité au profit d'une simple pénombre.
	(endif)

#burin

(item *)
(* is #in #salle)
(name *) burin
(descr *)
	Le burin est en relativement bon état. Presque sans surprise, sa pointe est argentée. S'agirait-il de celui qu'avait utilisé la femme du miroir ?

(appearance *)
	(ceiling is open)
	(* is pristine)
	Un burin traîne par terre, dans un coin.

#table

(f *)
(supporter *)
(* is #in #salle)
(name *) table
(descr *)
	Cette table en bois se trouve au fond de la salle, devant un vitrail. Un trou y est percé au centre.

(narrate putting #miroir #on *)
	J'insère le miroir dans la table; son pied entre parfaitement dans le trou.

(narrate taking #miroir)
	(#miroir is #on *)
	Je retire le miroir de l'autel.
	(now)~(vitrail is illuminated)

(instead of [put #miroir #in *])
	(try [put #miroir #on *])

(instead of [turn #miroir])
	(#miroir is #on *)
	(if)(table is pushed)(then)
		(if)(vitrail is complete)(then)
			Je tourne le miroir de façon à ce qu'il éclaire le vitrail complété. Il s'illumine. Je me retourne, et découvre que le tombeau est lui aussi illuminé d'une lumière blanche.
			(now)(vitrail is illuminated)
		(else)
			Je tourne le miroir de façon à ce qu'il éclaire le vitrail, mais rien ne se passe.
		(endif)
	(else)
		Je tourne le miroir dans tous les sens, mais rien ne se passe, puisque la table n'est pas éclairée par la lumière venant du plafond.
	(endif)

(prevent [push *])
	(if)(table is pushed)(then)
		La table est très bien où elle est, dans la lumière.
	(else)
		Je pousse la table de façon à ce qu'elle soit directement sous l'ouverture du plafond.
		(now)(table is pushed)
	(endif)

#vitrail

(* is #in #salle)
(name *) vitrail
(heads *) vitrail
(descr *)
	Un vitrail abstrait a été placé sur le mur du fond.
	(if)(vitrail is complete)(then)
		Il est complet.
	(else)
		Il lui manque quelques morceaux.
	(endif)

(vitrail is complete)
	(#morceau-jaune is #partof *)
	(#morceau-vert is #partof *)
	(#morceau-rouge is #partof *)

(prevent [put $O #in/#on *])
	(vitrail $O)
	~($O is #partof *)
	J'insère (le $O) dans le vitrail.
	(now)($O is #partof #vitrail)
	(if)(vitrail is complete)(then)
		Il est maintenant complet !
	(endif)

%%% La fin

(narrate leaving #salle #south)
	(vitrail is illuminated)
	À peine ai-je fait un pas dans le tombeau éclatant de lumière que je me fige. Le gisant n'est plus allongé. Il est assis. Ou alors elle — la Princesse spéculaire — est assise ?
	(par)
	(any key)
	Elle se regarde les mains, comme étonnée de ce qu'il lui arrive.
	(par)
	(any key)
	M'entendant arriver, elle lève la tête vers moi.
	(par)
	(any key)
	(game over { Fin })

(game over option)
	lire les NOTES de l'auteur,

(parse game over [note/notes])
	(par)
	Tout d'abord, merci d'avoir joué !
	(par)
	Je suis conscient que la fin n'est pas vraiment satisfaisante, mais j'ai malheureusement manqué de temps. Vous pouvez m'envoyer une transcription à natrium729\@gmail.com pour m'encourager à terminer l'écriture du jeu.
	(par)
	\(Et si vous avez trouver la fin satisfaisante, eh bien, tant mieux pour vous !\)
	(par)
	Une fois encore, merci !
	(par)
