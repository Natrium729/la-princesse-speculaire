# La Princesse spéculaire

Cette fiction interactive est ma participation au [concours de fiction interactive francophone 2022](https://itch.io/jam/concours-de-fiction-interactive-2022). Elle a été écrite avec Dialog.

J'ai écrit le gros du jeu en 2 jours après une subite motivation juste avant la date limite, il y a donc beaucoup d'améliorations à ajouter. Je m'y attellerai après la fin du concours.

[Lien vers la page itch.io du jeu.](https://natrium729.itch.io/la-princesse-speculaire)

## Compiler le jeu

Il vous faudra [Dialog](https://linusakesson.net/dialog/index.php) et les outils pour la [Å-machine](https://linusakesson.net/dialog/aamachine/index.php).

Pour compiler l'histoire en mode debug :

```
$ dialogc -t aa princesse-speculaire-debug.dg princesse-speculaire.dg ./cadratin/cadratin-debug.dg ./cadratin/cadratin-french.dg ./cadratin/cadratin.dg
```

Pour compiler l'histoire pour la publication :

```
$ dialogc -t aa princesse-speculaire.dg ./cadratin/cadratin-french.dg ./cadratin/cadratin.dg
```

En mode debug, il est possible de sauter l'introduction, d'utiliser les commandes de débogage usuelles de Dialog et de taper `test 1` et `test 2` en jeu pour résoudre les première et seconde parties, respectivement.

Pour générer la page web contenant la fiction interactive jouable en ligne :

```
aambundle -o princesse-speculaire-web princesse-speculaire.aastory
```

Où `princesse-speculaire.aastory` est le fichier de jeu créé par dialogc avec les commandes plus hauts.

## Cadratin

Cadratin est mon « fork personnel » de la stdlib de Dialog écrite par Linus Åkesson. Elle en dévie de façon significative à certains endroits.

Cadratin n'est pas encore terminée et n'est pas vraiment dans un état convenable : morceaux de la stdlib manquants, textes non traduits, textes en français dans le fichier principal et non dans `cadratin-french`… Tout cela parce que mon principal objectif était de terminer ma participation à temps pour le concours, et pas de faire du code propre. Cela sera remédié dans une mise à jour du jeu, et le dépôt de Cadratin sera rendu public quand il sera plus présentable.

## Licence

Pas de licence spécifiée pour le moment, mais si vous souhaitez vous inspirer du code (pas qu'il soit extraordinaire non plus), je n'ai pas de problème avec ça. Dans le doute, contactez-moi !

La licence de Cadratin est dans le dossier `cadratin`.
